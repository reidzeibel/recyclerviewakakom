package id.akakom.recyclerviewproject;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public class MahasiswaAdapter extends RecyclerView.Adapter<MahasiswaHolder> {

    // buat nama
    List<Mahasiswa> daftarMahasiswaUntukAdapter;

    public MahasiswaAdapter(List<Mahasiswa> daftarMahasiswaDariClassLain) {
        daftarMahasiswaUntukAdapter = daftarMahasiswaDariClassLain;
    }

    @Override
    public MahasiswaHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =
                LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.item_layout, parent, false);
        MahasiswaHolder holder = new MahasiswaHolder(view);
        return holder;
    }

    // Menyambungkan antara data dengan ViewHolder
    // position adalah posisi item pada RecyclerView
    @Override
    public void onBindViewHolder(MahasiswaHolder holder, int position) {
        // Ambil data mahasiswa pada posisi data tertentu (0,1,2,dst)
        Mahasiswa mahasiswa = daftarMahasiswaUntukAdapter.get(position);

        // sambungkan viewHolder dengan data, masukkan nilai pada data kedalam
        // view yang sudah disiapkan pada ViewHolder
        holder.nim.setText(mahasiswa.nim);
        holder.nama.setText(mahasiswa.nama);
    }

    @Override
    public int getItemCount() {
        return daftarMahasiswaUntukAdapter.size();
    }
}
