package id.akakom.recyclerviewproject;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class MahasiswaHolder extends RecyclerView.ViewHolder {

    TextView nim;
    TextView nama;

    public MahasiswaHolder(View itemView) {
        super(itemView);
        nim = (TextView) itemView.findViewById(R.id.nim);
        nama = (TextView) itemView.findViewById(R.id.nama);
    }
}
