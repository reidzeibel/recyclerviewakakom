package id.akakom.recyclerviewproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inisialisasi Daftar
        // membuat sebuah daftar dengan nama daftar : "daftarMahasiswa"
        // "new ArrayList<>()" digunakan untuk melakukan inisialisasi
        // daftar baru yang masih kosong
        List<Mahasiswa> daftarMahasiswa = new ArrayList<>();

        // Membuat Objek Mahasiswa
        Mahasiswa ridwan = new Mahasiswa("12345", "Ridwan");
        Mahasiswa ali = new Mahasiswa("11769", "Ali");
        Mahasiswa faris = new Mahasiswa("11798", "Faris");
        Mahasiswa danang = new Mahasiswa("11450", "Danang");
        Mahasiswa indra = new Mahasiswa("11673", "Indra");
        Mahasiswa dimas = new Mahasiswa("12245", "Dimas");
        Mahasiswa andre = new Mahasiswa("11345", "Andre");
        Mahasiswa tomi = new Mahasiswa("11945", "Tomi");

        // tambahkan mahasiswa ke dalam daftar yang sudah dibuat
        daftarMahasiswa.add(ridwan);
        daftarMahasiswa.add(ali);
        daftarMahasiswa.add(faris);
        daftarMahasiswa.add(danang);
        daftarMahasiswa.add(indra);
        daftarMahasiswa.add(dimas);
        daftarMahasiswa.add(andre);
        daftarMahasiswa.add(tomi);

        RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerView);
        MahasiswaAdapter adapter = new MahasiswaAdapter(daftarMahasiswa);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(adapter);

    }
}
